var _ = require('underscore');
var path = require('path');
var fs = require("fs");

var _environment;

function getFileFullPath(configFileName) {
  return path.normalize(path.dirname(Object.keys(require.cache).filter(function (pathtr) {
    return fs.existsSync(path.dirname(pathtr) + '/' + configFileName);
  })[0]) + '/' + configFileName);
}

var getNormalizedPath = function (configFileName) {
  var fullPath = getFileFullPath(configFileName);
  if(!fullPath) throw new Error("No such file:" + configFileName);
  return fullPath;
};

module.exports.setEnvironment = function (environment) {
  _environment = environment;
};

module.exports.load = function (configFileName) {

  var configFileParts = configFileName.split(".");
  configFileParts.splice(configFileParts.length - 1, 0, _environment);

  var subConfigFileName = configFileParts.join(".");
  var subConfig = {};
  var config = {};

  try {
    subConfig = require(getNormalizedPath(subConfigFileName));
  } catch (e) {
  }
  try {
    config = require(getNormalizedPath(configFileName));
  } catch (e) {
  }

  return _.extend(
    config,
    subConfig
  );
};