var assert = require('assert');

var testRequiringLoader = require("./another/test-requiring-loader");

describe('Environment config loader', function () {
  it('still remembers set environment even if set from another file which one is in other dir', function () {

    var values = testRequiringLoader();

    assert.deepEqual(values, {
      "key1": "value1",
      "key2": "value2",
      "key3": "value3"
    });

  })
});