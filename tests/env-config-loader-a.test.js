var assert = require("assert");

var moduleName = '../env-config-loader';

var loader = require(moduleName);

describe('Environment config loader', function() {

    it('loads simple config file', function() {
        var values = loader.load('./static/test1.json');
        assert.deepEqual(values, {"testKey": "testValue"});
    });

    it('replaces configuration values if environment is provided', function() {
        loader.setEnvironment("def"); // to make sure it's different from others
        var values = loader.load('./static/test2.json');
        assert.deepEqual(values, {
            "key1": "value1",
            "key2": "value4",
            "key3": "value5"
        });
    });

    it('persists environment variable from import to import', function() {
        var loader2 = require(moduleName);
        var values = loader2.load('./static/test2.json');
        assert.deepEqual(values, {
            "key1": "value1",
            "key2": "value4",
            "key3": "value5"
        });
    });
});